$(document).ready(function(){
	window.player = $('#player audio')[0]
	window.random = false
	window.repeat = false
	$.ajax({
		method: "GET",
		url: "getSongs.php",
		dataType: "json",
		success: function(data){
			window.songs = data
			if (data.length > 0){
				$('#player audio source').attr("src", data[0])
				player.load()
				playPause()
				for (var i = 0; i < songs.length; i++) {
					var a = (i == 0) ? '" class="active"' : '"'
					$('#playlist').append('<a onclick="change(this)" song="' + songs[i] + a + '>' + songs[i].replace('songs/', '') + '</a><br>')
				}
			}
		}
	})
	player.addEventListener("ended", function(){
		console.log('ended')
		player.currentTime = 0
		trackControl(1)
	})
})
function change(ini){
	var src = $(ini).attr('song')
	var filename = $(ini).html()
	var filename = filename.replace("." + filename.split('.').pop(), "")
	$('title').html(filename)
	$('.active').removeClass('active')
	$(ini).addClass('active')
	$('#player audio source').attr("src", src)
	playPause(1)
}
function playPause(a){
	var control = $('control span:first')
	if (a == 1) {
		player.load()
	}
	if (player.paused == false) {
		player.pause()
		control.html('[PLAY]')
	} else {
		player.play()
		control.html('[PAUSE]')
	}
}
function stopPlayer(){
	$('control span:first').html('[PLAY]')
	player.pause()
	player.currentTime = 0
}
function seekPlayer(seek, from){
	var curent = player.currentTime
	if (from == 'slide'){
		var total = player.duration
		seek = parseInt(seek)
		player.currentTime = seek * total / 100
	}else{
	player.currentTime = curent + seek
	}
}
function trackControl(to){
	function getRandomIntInclusive(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	var total = $('#playlist a').length
	var playlist = $('#playlist a.active')
	var index = playlist.index() / 2
	var clicked = 0
	if (to == -1 && index == 0){
		clicked = total - 1
	}else if(to == 1 && index == (total - 1)){
		clicked = 0
	}else if(random == true){
		clicked = getRandomIntInclusive(0, (total - 1))
	}else{
		clicked = index + to
	}
	if(repeat == true){
		clicked = index
	}
	$('#playlist a')[clicked].click()
}
function toggleRand(){
	if (random == false){
		window.random = true
		$('#randPlay').html('[RANDOM]')
	}else{
		window.random = false
		$('#randPlay').html('<strike>[RANDOM]</strike>')
	}
}
function toggleReap(){
	if (repeat == false){
		window.repeat = true
		$('#reapPlay').html('[REPEAT]')
	}else{
		window.repeat = false
		$('#reapPlay').html('<strike>[REPEAT]</strike>')
	}
}
function curTime(ini){
	function formatTime(seconds) {
		minutes = Math.floor(seconds / 60);
		minutes = (minutes >= 10) ? minutes : "0" + minutes;
		seconds = Math.floor(seconds % 60);
		seconds = (seconds >= 10) ? seconds : "0" + seconds;
		return minutes + ":" + seconds;
	}
	var cur = Math.floor(ini.currentTime)
	var dur = Math.floor(ini.duration)
	$('#currentTime').html(formatTime(cur) + " / " + formatTime(dur))
}