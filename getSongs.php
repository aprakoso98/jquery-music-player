<?php
$dir = "songs/";
$songs = array();
if (is_dir($dir)){
	if ($dh = opendir($dir)){
		$i = 1;
		while (($file = readdir($dh)) !== false){
			if (!in_array($file, array('.', '..'))){
				$song = $dir.$file;
				array_push($songs, $song);
			};
		};
		closedir($dh);
	}
}
$songs = json_encode($songs);
echo $songs;
?>