<!DOCTYPE html>
<html>
<head>
	<title>Play your music</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="main.js"></script>
</head>
<body>
	<div id="playlist"></div>
	<div id="player">
		<audio controls style="display: none;" ontimeupdate="curTime(this)">
			<source src="" type="audio/mpeg"></source>
			Your browser does not support the audio element.
		</audio>
	</div>
	<div id="view">
		<control>
			<span onclick="playPause()">[PLAY]</span>
			<span onclick="stopPlayer()">[STOP]</span>
			<span onclick="seekPlayer(-30)">[-30s]</span>
			<span onclick="seekPlayer(30)">[+30s]</span>
			<span onclick="trackControl(-1)">[PREV]</span>
			<span onclick="trackControl(1)">[NEXT]</span>
			<span id="randPlay" onclick="toggleRand()"><strike>[RANDOM]</strike></span>
			<span id="reapPlay" onclick="toggleReap()"><strike>[REPEAT]</strike></span>
			<span id="currentTime">00:00 / 00:00</span>
			<input type="range" id="silder" onchange="seekPlayer(this.value, 'slide')" value="0" min="0" max="100">
		</control>
	</div>
</body>
</html>